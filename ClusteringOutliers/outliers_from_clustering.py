""" This module will plot outliers in a 2d graph based
on selected Clustering Algorithm"""

import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import silhouette_score, silhouette_samples
from sklearn.datasets import fetch_openml, make_blobs
from sklearn.decomposition import PCA
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA
import hdbscan
import pandas as pd
import umap.umap_ as umap
import plotly.graph_objects as go


class ClusteringOutlierdetection():
    """
    Identifies outliers via different clustering algorithms
    """

    # pylint: disable=R0913
    def __init__(self, input_data='mnist', embedding_technique='PCA', clustering_technique='KMEANS',
                 kmeans_detection_method='silhouette', min_clusters=10):
        self.input_data = input_data
        self.embedding_technique = embedding_technique
        self.clustering_technique = clustering_technique
        self.kmeans_detection_method = kmeans_detection_method
        self.min_clusters = min_clusters

    def load_data(self):
        """
        load entire data, process samples for dimensionality reduction holding 98% variance,
        scale the image_data return scaled image_data, label data, reduced feature set
        """

        if self.input_data == 'mnist':
            dataset = fetch_openml('mnist_784', version=1)
            x_train, y_train = dataset['data'], dataset['target']
            y_train = y_train.astype('int32')  # changing the datatype from str to int
            cols = ['pixel' + str(i) for i in range(1, x_train.shape[1] + 1)]
            dataframe = pd.DataFrame(x_train, columns=cols)
            dataframe['y'] = y_train
            dataframe['label'] = pd.Categorical(dataframe['y'])

            # Random sampling
            np.random.seed(42)
            perm = np.random.permutation(dataframe.shape[0])  # random index generation
            # using only first 10000 data points
            data = dataframe.loc[perm[:1000], :].copy()
            data.reset_index(drop=True, inplace=True)

            # dimensionality reduction holding 98percent variance
            pca = PCA(n_components=.98)
            pca_features = pca.fit_transform(data[cols].values)
            data_scaling = data[cols]
            y_train = data['y']

        elif self.input_data == 'make_blob':
            # pylint: disable=unbalanced-tuple-unpacking
            x_train, y_train = make_blobs(n_samples=[500, 200, 800, 200],
                                          n_features=4,
                                          cluster_std=[.3, .70, .75, .8],
                                          shuffle=True,
                                          random_state=11,
                                          )
            data_scaling = x_train.copy()
        else:
            return "Entered input dataset cant be processed"

        # Scaling the data
        scaler = StandardScaler()
        x_train = scaler.fit_transform(data_scaling)

        if self.input_data == 'mnist':
            print(x_train.shape)
            return x_train, y_train, pca_features
        return x_train, y_train, x_train

    @staticmethod
    def plot_2d(train, y_train):
        """
         helper function for method embedding_2d, plot 2d graph
        :param: input data
        :param: y_train

        """
        fig = go.Figure(data=go.Scatter(x=train[:, 0], y=train[:, 1],
                                        mode='markers',
                                        marker=dict(
                                            size=20,
                                            color=y_train,
                                            colorscale='Rainbow',
                                            showscale=True,
                                            line_width=1)
                                        ))
        fig.update_layout(margin=dict(l=50, r=50, b=50, t=50), width=1000, height=1200)
        fig.layout.template = 'plotly_dark'
        fig.show()

    @staticmethod
    def plot_3d(train, y_train):
        """
        helper function for method embedding_2d, plot 3d graph
        :param: input data
        :param: y_train

        """
        fig = go.Figure(data=go.Scatter3d(x=train[:, 0], y=train[:, 1], z=train[:, 2],
                                          mode='markers',
                                          marker=dict(
                                              size=8,
                                              color=y_train,
                                              colorscale='Rainbow',
                                              opacity=1,
                                              line_width=1)
                                          ))
        fig.update_layout(margin=dict(l=50, r=50, b=50, t=50), width=1000, height=1200)
        fig.layout.template = 'plotly_dark'
        fig.show()

    def embedding_2d(self, pca_features, y_train):
        """
        take reduced_data , apply embedding technique.
        :param pca_features:
        :param y_train:
        :return: 2d embedded data
        """

        if self.embedding_technique == 'PCA':
            reducer = PCA(n_components=2, random_state=0)  # 2d_Embedding
            train_reducer = reducer.fit_transform(pca_features)
            reducer3d = PCA(n_components=3, random_state=343)  # 3d_Embedding
            train_reducer3d = reducer3d.fit_transform(pca_features)

        elif self.embedding_technique == 'UMAP':
            reducer = umap.UMAP(n_components=2, random_state=343)  # 2d_Embedding
            train_reducer = reducer.fit_transform(pca_features)
            reducer3d = umap.UMAP(n_components=3, random_state=343)  # 3d_Embedding
            train_reducer3d = reducer3d.fit_transform(pca_features)
        else:
            reducer = LDA(n_components=2)  # 2d_Embedding
            train_reducer = reducer.fit_transform(pca_features, y_train)
            reducer3d = LDA(n_components=3)  # 3d_Embedding
            train_reducer3d = reducer3d.fit_transform(pca_features, y_train)

        # Plotting 3d graph and 2d Graph of the data
        self.plot_2d(train_reducer, y_train)
        self.plot_3d(train_reducer3d, y_train)
        return train_reducer

    @staticmethod
    def _helper_get_kmeans_ideal_cluster_number(pca_features, min_clusters):
        silhouette_dict = {}
        for cluster in range(min_clusters, 50):
            kmeans = KMeans(n_clusters=cluster)
            kmeans.fit(pca_features)
            silhouette_dict[cluster] = silhouette_score(pca_features, kmeans.labels_)

        ideal_cluster_number = max(zip(silhouette_dict.values(), silhouette_dict.keys()))[1]
        return ideal_cluster_number

    @staticmethod
    def _helper_get_kmeans_outliers_centroid_distance_labels(labels, x_digits_dist, pca_features,
                                                             ideal_cluster_number, y_train):
        # finding the instances closest to the created clusters' centers
        representative_digit_idx = np.argmin(x_digits_dist, axis=0)
        # labels closest to created clusters' centers
        y_representative_digits = y_train[representative_digit_idx]
        y_train_propagated = np.empty(len(pca_features), dtype=np.int32)
        for i in range(ideal_cluster_number):
            try:
                y_train_propagated[labels == i] = y_representative_digits[i]
            except KeyError:
                print(f"Cant detect the label {i} ")
        # 90percentile of the data close to centroid considered as clustered data
        percentile = 90
        x_cluster_dist = x_digits_dist[np.arange(len(pca_features)), labels]

        for i in range(ideal_cluster_number):
            in_cluster = (labels == i)
            cluster_dist = x_cluster_dist[in_cluster]
            cut_off_distance = np.percentile(cluster_dist, percentile)
            above_cutoff = x_cluster_dist > cut_off_distance
            labels[in_cluster & above_cutoff] = -1
        return labels, x_cluster_dist

    @staticmethod
    def _helper_get_kmeans_outliers_silhouette_labels(labels, sample_silhouette_values,
                                                      ideal_cluster_number):
        percentile = 10
        for i in range(ideal_cluster_number):
            # Aggregate the silhouette scores for samples belonging to
            # cluster i, and sort them
            ith_cluster_silhouette_values = sample_silhouette_values[labels == i]
            sorted_ith_cluster_silhouette_values = np.sort(ith_cluster_silhouette_values)[::-1]
            # ith_cluster_silhouette_values.sort()

            cut_off_distance = np.percentile(sorted_ith_cluster_silhouette_values, percentile)
            below_cutoff = sample_silhouette_values < cut_off_distance
            labels[(labels == i) & below_cutoff] = -1
        membership_score = sample_silhouette_values
        return labels, membership_score

    def clustering(self, pca_features, y_train,x_train, kmeans_n_clusters):
        """
        apply clustering technique on scaled input data
        :param pca_features: input data post dimensionality reduction PCA Technique
        :param y_train: label data
        :return: cluster labels and cluster membership score
        """

        if self.clustering_technique == 'KMEANS':
            if kmeans_n_clusters:
                kmeans = KMeans(n_clusters=kmeans_n_clusters).fit(pca_features)
            else:
                kmeans_n_clusters = self._helper_get_kmeans_ideal_cluster_number\
                    (pca_features, self.min_clusters)
                kmeans = KMeans(n_clusters=kmeans_n_clusters).fit(pca_features)

            raw_labels = kmeans.labels_
            if self.kmeans_detection_method == 'silhouette':
                sample_silhouette_values = silhouette_samples(pca_features, raw_labels)
                labels, membership_score = self._helper_get_kmeans_outliers_silhouette_labels(
                    kmeans.labels_, sample_silhouette_values, kmeans_n_clusters)
            else:
                x_digits_dist = kmeans.transform(pca_features)
                labels, membership_score = \
                    self._helper_get_kmeans_outliers_centroid_distance_labels(
                        raw_labels, x_digits_dist, pca_features, kmeans_n_clusters, y_train)

        else:
            if self.input_data == 'mnist':
                data = PCA(n_components=50).fit_transform(x_train)
            else:
                data = x_train.copy()
            hdb_scan = hdbscan.HDBSCAN(alpha=1.0, min_samples=2,
                                       min_cluster_size=5).fit(data)
            membership_score = hdb_scan.outlier_scores_
            raw_labels = hdb_scan.labels_
            labels = raw_labels
            # Alternate: hdb_scan.probabilities
        return labels, membership_score

    @staticmethod
    def _helper_get_clustered_outliers_dataset_cluster_label(
            train_pca, y_train, cluster_labels=None, membership_score=None):
        """
        :param train_pca: 2d Embedded Input data
        :param y_train: dataset labels
        :param cluster_labels: labels identified by clustering algorithm
        :param membership_score: membership_score/outlier_score calculated by clustering algorithm
        :return: dataset which are clustered with no outliers and another dataset
        consists of only outliers based on cluster_labels or membership score
        """
        if membership_score is None:
            x_train_clustered = train_pca[cluster_labels != -1]
            x_train_outlier = train_pca[cluster_labels == -1]
            y_train_clustered = y_train[cluster_labels != -1]
        else:
            threshold = pd.Series(membership_score).quantile(0.9)
            outliers = np.where(membership_score > threshold)[0]
            clustered = np.where(membership_score < threshold)[0]
            x_train_clustered = train_pca[clustered]
            x_train_outlier = train_pca[outliers]
            y_train_clustered = y_train[clustered]
        return x_train_clustered, x_train_outlier, y_train_clustered

    def plot_outliers_clusters(self, kmeans_n_clusters=None):
        """
        Calls load_data, embedding_2d,and clustering methods of the class
        Obtain dataframe with clustered points and dataframe with outliers
        Plot graph for them
        """
        # loading the data
        x_train, y_train, pca_features = self.load_data()
        # applying embedding technique
        train_pca = self.embedding_2d(pca_features, y_train)
        # clustering algo
        cluster_labels, membership_score = self.clustering(pca_features, y_train, x_train,
                                                           kmeans_n_clusters)

        # Outlier Detection: Clustered and Outlier dataset generation
        if self.clustering_technique in ('KMEANS', 'DBSCAN'):
            x_train_clustered, x_train_outlier, y_train_clustered = \
                self._helper_get_clustered_outliers_dataset_cluster_label(
                    train_pca, y_train, cluster_labels=cluster_labels)
        else:
            x_train_clustered, x_train_outlier, y_train_clustered = \
                self._helper_get_clustered_outliers_dataset_cluster_label(
                    train_pca, y_train, membership_score=membership_score)

        # PLOTTING
        # Create a subplot with 1 row and 2 columns
        fig, (ax1, ax2) = plt.subplots(2, 1)
        fig.set_size_inches(15, 10)

        # Cluster Visualisation with outliers
        ax1.scatter(x_train_clustered[:, 0], x_train_clustered[:, 1],
                    c="red", s=20, alpha=0.5, linewidth=0)
        ax1.scatter(x_train_outlier[:, 0], x_train_outlier[:, 1],
                    c="black", s=20, linewidth=0, alpha=0.5)
        ax1.set_title('Outlier Detection With Clustering')
        ax1.set_xlabel("Projected Component1")
        ax1.set_ylabel("Projected Component2")

        # Cluster Visualisation
        ax2.set_title('Cluster Visualisation with No Outlier')
        for _ in range(len(x_train)):
            ax2.scatter(x_train_clustered[:, 0], x_train_clustered[:, 1],
                        c=y_train_clustered, s=20, cmap='jet')

        plt.show()
