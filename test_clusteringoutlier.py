import numpy as np
from ClusteringOutliers import outliers_from_clustering

input_parameter = {
    "valid_dataset": ["mnist", "make_blob"],
    "valid_embedding_technique": ["PCA", "UMAP"],
    "valid_clustering_algo": ["KMEANS", "HDBSCAN"],
    "kmeans_detection_method": ["silhouette", "centroid"]
        }

output_load_data_shape = {
    "mnist": 784,
    "make_blob": 4,
    "mnist_features": 216  # 10000 instances select 256 and 1000 instances select 216
}
output_2d_embedded_shape = {
    "dimension": 2
}

# mnist loading dataset
x_train_mnist, y_train_mnist, pca_features_mnist = outliers_from_clustering.ClusteringOutlierdetection(
    input_data=input_parameter["valid_dataset"][0],
    embedding_technique=input_parameter["valid_embedding_technique"][1],
    clustering_technique=input_parameter["valid_clustering_algo"][0],
    kmeans_detection_method=
    input_parameter["kmeans_detection_method"][0]).load_data()
# make_blob loading data set
x_train_make_blob, y_train_make_blob, _ = outliers_from_clustering.ClusteringOutlierdetection(
    input_data=input_parameter["valid_dataset"][1],
    embedding_technique=input_parameter["valid_embedding_technique"][1],
    clustering_technique=input_parameter["valid_clustering_algo"][0],
    kmeans_detection_method=
    input_parameter["kmeans_detection_method"][0]).load_data()


def test_load_data_output_shape_validations():

    assert outliers_from_clustering.ClusteringOutlierdetection(
        input_data=input_parameter["valid_dataset"][1],
        embedding_technique=input_parameter["valid_embedding_technique"][0],
        clustering_technique=input_parameter["valid_clustering_algo"][0],
        kmeans_detection_method=
        input_parameter["kmeans_detection_method"][0]).load_data()[0].shape[1] == output_load_data_shape["make_blob"]

    assert outliers_from_clustering.ClusteringOutlierdetection(
        input_data=input_parameter["valid_dataset"][0],
        embedding_technique=input_parameter["valid_embedding_technique"][1],
        clustering_technique=input_parameter["valid_clustering_algo"][0],
        kmeans_detection_method=
        input_parameter["kmeans_detection_method"][1]).load_data()[0].shape[1] == output_load_data_shape["mnist"]

    assert outliers_from_clustering.ClusteringOutlierdetection(
        input_data=input_parameter["valid_dataset"][0],
        embedding_technique=input_parameter["valid_embedding_technique"][1],
        clustering_technique=input_parameter["valid_clustering_algo"][0],
        kmeans_detection_method=
        input_parameter["kmeans_detection_method"][0]).load_data()[2].shape[1] == \
        output_load_data_shape["mnist_features"]


def test_2d_embedded_data_output_shape_validations():
    # mnist 2d embedding output shape
    assert outliers_from_clustering.ClusteringOutlierdetection(
        input_data=input_parameter["valid_dataset"][0],
        embedding_technique=input_parameter["valid_embedding_technique"][1],
        clustering_technique=input_parameter["valid_clustering_algo"][0],
        kmeans_detection_method=
        input_parameter["kmeans_detection_method"][0]).embedding_2d(pca_features_mnist, y_train_mnist).shape[1] == \
        output_2d_embedded_shape["dimension"]

    # make_blob 2d embedding output shape
    assert outliers_from_clustering.ClusteringOutlierdetection(
        input_data=input_parameter["valid_dataset"][1],
        embedding_technique=input_parameter["valid_embedding_technique"][1],
        clustering_technique=input_parameter["valid_clustering_algo"][0],
        kmeans_detection_method=
        input_parameter["kmeans_detection_method"][0]).embedding_2d(x_train_make_blob, y_train_make_blob).shape[1] == \
        output_2d_embedded_shape["dimension"]


def test_clustering_output_validations():
    result1, result2 = outliers_from_clustering.ClusteringOutlierdetection(
        input_data=input_parameter["valid_dataset"][1],
        embedding_technique=input_parameter["valid_embedding_technique"][0],
        clustering_technique=input_parameter["valid_clustering_algo"][1]).clustering(pca_features_mnist, y_train_mnist)
    assert type(result1) == np.ndarray
    assert type(result2) == np.ndarray


def test_plot_outliers_clusters_validation():
    outliers_from_clustering.ClusteringOutlierdetection(
        input_data=input_parameter["valid_dataset"][1],
        embedding_technique=input_parameter["valid_embedding_technique"][1],
        clustering_technique=input_parameter["valid_clustering_algo"][0],
        kmeans_detection_method=
        input_parameter["kmeans_detection_method"][0]).plot_outliers_clusters()
    assert True



